<?php include_once '../config.php'; ?>

<?php
    $success = false;
    if(isset($_POST['delete_package'])){
        $package_id = $_POST['package_id'];
        $SQL = $MySQLiconn->query("DELETE FROM packages WHERE package_id = {$package_id}");
        if(!$SQL){
            die($MySQLiconn->error);
        } 
        $success = true;
        $message = "You have deleted the package succesfully";
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
    if(isset($_POST['add_package'])){
            $target_dir = "../uploads/";
            $target_file = $target_dir . basename($_FILES["filename"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            $filename="";
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["filename"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    die("File is not an image.");
                    $uploadOk = 0;
                }
            }
        
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                die("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                die("Sorry, your file was not uploaded.");
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file)) {
                    $filename = basename( $_FILES["filename"]["name"]);
                } 
            }

            $title = $MySQLiconn->real_escape_string($_POST['title']);
            $place = $MySQLiconn->real_escape_string($_POST['place']);
            $description = $MySQLiconn->real_escape_string($_POST['description']);
            $price_per_head = $MySQLiconn->real_escape_string($_POST['price_per_head']);
            $tour_details = nl2br($_POST['tour_details']);
            $MySQLiconn->begin_transaction();
            $SQL = $MySQLiconn->query("INSERT INTO packages(title,description,price_per_head,tour_details,place,filename) VALUES('{$title}','{$description}',{$price_per_head},'{$tour_details}','{$place}','{$filename}')");
            if(!$SQL){
                die($MySQLiconn->error);
            } 

            $MySQLiconn->commit();
            $success = true;
            $message = "You have add packages succesfully";
            header('Location: '.$_SERVER['HTTP_REFERER']);
    }

    if(isset($_POST['update_package'])){
                      $target_dir = "../uploads/";
            $target_file = $target_dir . basename($_FILES["filename"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
            $filename="";
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["filename"]["tmp_name"]);
                if($check !== false) {
                    $uploadOk = 1;
                } else {
                    die("File is not an image.");
                    $uploadOk = 0;
                }
            }
        
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                die("Sorry, only JPG, JPEG, PNG & GIF files are allowed.");
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                die("Sorry, your file was not uploaded.");
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file)) {
                    $filename = basename( $_FILES["filename"]["name"]);
                } 
            }


            $package_id = $MySQLiconn->real_escape_string($_POST['package_id']);
            $title = $MySQLiconn->real_escape_string($_POST['title']);
            $place = $MySQLiconn->real_escape_string($_POST['place']);
            $description = $MySQLiconn->real_escape_string($_POST['description']);
            $price_per_head = $MySQLiconn->real_escape_string($_POST['price_per_head']);
            $price_per_head = $MySQLiconn->real_escape_string($_POST['price_per_head']);
            $tour_details = html_entity_decode(nl2br($_POST['tour_details']));
            $MySQLiconn->begin_transaction();
            $SQL = $MySQLiconn->query("UPDATE packages SET filename='{$filename}',title = '{$title}',description = '{$description}',price_per_head = {$price_per_head},tour_details = '{$tour_details}',place = '{$place}' WHERE package_id = {$package_id}");
            if(!$SQL){
                die($MySQLiconn->error);
            } 

            $MySQLiconn->commit();
            $success = true;
            $message = "You have saved packages succesfully";
    }


     $packages = mysqli_query($MySQLiconn,"SELECT * FROM packages") or  die($MySQLiconn->error);
     
?>  
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    
    <div id="wrapper">
    <?php include '../common/admin_header.php'; ?>

        <div id="page-wrapper" style="height:1000px;">
            <div class="container-fluid">
            <table class="table">
                <?php if($success): ?>
                <div class="alert alert-success">
                  <strong>Success!</strong><?=$message?>
                </div>
                <?php endif;?>
            	<caption><h4 style="text-align: left;color:#000;cursor:pointer;">Packages <a class="pull-right text-success" data-toggle="modal" data-target="#add_modal" ><i class="fa fa-plus"></i></a></h4>.</caption>
            	<thead>
            		<tr>
                        <th>Place</th>
            			<th>Title</th>
            			<th>Description</th>
            			<th>Price per head</th>
            			<th>Tour Details</th>
                        <th class="text-center">Action</th>
            		</tr>
            	</thead>
            	<tbody>
                <?php while($rows = mysqli_fetch_array($packages)):  ?> 

            		<tr>
                        
                        <td><?=$rows['place']?></td>
            			<td><?=$rows['title']?></td>
            			<td><?=$rows['description']?></td>
            			<td>Php. <?=$rows['price_per_head']?></td>
                        <td><?=$rows['tour_details']?></td>
                        <td  class="text-center">
                             <form method="post" action="<?=BASE_URI?>admin/update_package.php">
                            <input type="hidden" name="package_id" value="<?=$rows['package_id']?>">
                            
                            <button name="update_package" type="submit" class="btn btn-primary">Update</button>
                            </form>
                            
                            <?php include '../common/delete_modal.php'; ?>              

                            <a class="btn btn-danger"  data-toggle="modal" data-target="#delete_modal">Delete</a>
                        </td>
            		</tr>
                <?php endwhile; ?>
            	</tbody>
            </table>
            </div>
         </div>

    </div>
    <?php include '../common/update_modal.php'; ?>
    
    <?php include '../common/add_modal.php'; ?>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>

    </body>


</html>
