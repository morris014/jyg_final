<?php include_once '../config.php'; ?>
<?php include_once '../email.php'; ?>
<?php $success = false ; ?>
<?php


     $query  = "SELECT * FROM customer_reservation cr 
                LEFT JOIN packages p ON p.package_id = cr.package_id 
                ORDER BY cr.date_created DESC,cr.date_updated DESC
            ";
     $packages = mysqli_query($MySQLiconn,$query) or  die($MySQLiconn->error);

     if(isset($_POST['submit'])){
        $reservation_id = $_POST['reservation_id'];
        $email = $_POST['email'];
        $package_id = $_POST['package_id'];
        
         $query = "UPDATE customer_reservation SET status='Verified' WHERE reservation_id = {$reservation_id}"; 
         $result = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 
         if($result){ 
           if($email){
             sendEmail(); 
           }
           
           echo "<script>alert('Saved Succesfully'); window.location.replace('reservation.php');</script>";
         }
         
     }   

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    
    <div id="wrapper">
    <?php include '../common/admin_header.php'; ?>

        <div id="page-wrapper" style="height:1000px;">
            <div class="container-fluid">
            <table class="table">
                <?php if($success): ?>
                <div class="alert alert-success">
                  <strong>Success!</strong><?=$message?>
                </div>
                <?php endif;?>
            	<caption><h4 style="text-align: left;color:#000;cursor:pointer;">Customer Reservation <a class="pull-right text-success" data-toggle="modal" data-target="#add_modal" ><i class="fa fa-plus"></i></a></h4>.</caption>
            	<thead>
            		<tr>
                        <td>Package No</td>
                        <th>Email</th>
                        <th>First Name</th>
            			<th>Last Name</th>
            			<th>Address</th>
            			<th>Contact No</th>
            			<th>No of Adults</th>
                        <th>No of Infants</th>
                        <th>Status</th>
                        <th class="text-center">Action</th>
            		</tr>
            	</thead>
            	<tbody>
                <?php while($rows = mysqli_fetch_array($packages)):  ?> 

            		<tr>
                        <td><a target="_blank" href="<?=BASE_URI?>book.php?package=<?=$rows['package_id']?>"><?=$rows['package_id']?></a></td>
                        <td><?=$rows['email']?></td>
                        <td><?=$rows['first_name']?></td>
            			<td><?=$rows['last_name']?></td>
            			<td><?=$rows['address']?></td>
                        <td><?=$rows['contact_no']?></td>
            			<td><?=$rows['no_of_adults']?></td>
                        <td><?=$rows['no_of_infants']?></td>
                        <td><?=$rows['status']?></td>
                        <td  class="text-center">
                            <form method="post">
                                <input type="hidden" name="reservation_id" value="<?=$rows['reservation_id']?>">
                                <input type="hidden" name="email" value="<?=$rows['email']?>">
                                <input type="hidden" name="package_id" value="<?=$rows['package_id']?>">
                                <button class="btn btn-primary" name="submit" type="submit">Confirm</button>  
                            </form>
                        </td>

            		</tr>
                <?php endwhile; ?>
            	</tbody>
            </table>
            </div>
         </div>

    </div>
    <?php include '../common/update_modal.php'; ?>
    <?php include '../common/delete_modal.php'; ?>
    <?php include '../common/add_modal.php'; ?>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>

    </body>


</html>
