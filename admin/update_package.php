<?php include_once '../config.php'; ?>
<?php
    $success = false;


    if(!isset($_POST['update_package'])){
      Header('Location: '.BASE_URI.'admin');
    }

    if(!isset($_POST['package_id'])){
      Header('Location: '.BASE_URI.'admin');
    }
    $package_id = (int) $_POST['package_id'];
    if($package_id < 0){
      Header('Location: '.BASE_URI.'admin'); 
    }
    $details = mysqli_query($MySQLiconn,"SELECT * FROM packages WHERE package_id = {$package_id}") or  die($MySQLiconn->error);

    $package = mysqli_fetch_assoc($details);




?>  
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    
    <div id="wrapper">
    <?php include '../common/admin_header.php'; ?>

        <div id="page-wrapper" style="height:1000px;">
            <div class="container-fluid">
                <div id="update_modal" class="">
                  <div class="modal-dialog modal-sm" style="width: 35%;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="font-size:13px;">Update Package</h4>
                      </div>
                      <div class="modal-body" style="padding:20px 15px;">
                        <div class="row">
                        <form method="post" action="<?=BASE_URI?>admin/index.php" enctype="multipart/form-data">
                          <input type="hidden" name="package_id" value="<?=$package_id?>">
                            <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <img src= "../uploads/<?=$package['filename']?>">
                           </div>
                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                              <input type="file" class="form-control" placeholder="Change Image" name="filename" required>
                          </div>
                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                            <input class="form-control" placeholder="Place" name="place" value="<?=$package['place']?>" required>
                          </div>
                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                            <input class="form-control" placeholder="Title" name="title" value="<?=$package['title']?>"  required>
                          </div>
                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                            <input class="form-control" placeholder="Description" name="description" value="<?=$package['description']?>"  required>
                          </div>
                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                            <input type="number" class="form-control" placeholder="Price Per Head" value="<?=$package['price_per_head']?>"  name="price_per_head" required>
                          </div>

                          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                            <pre><textarea rows="30" cols="100" class="form-control" placeholder="Tour Details" name="tour_details" required><?=br2nl($package['tour_details'])?></textarea></pre>
                          </div>



                          <div class="text-center col-sm-12" style="margin-top:25px;">
                            <button type="submit" name="update_package" class="btn btnf-f btn-success btn-block">SAVE</button>
                          </div>
                        </form>          
                        </div>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


            </div>
         </div>

    </div>
    <?php include '../common/update_modal.php'; ?>
    <?php include '../common/delete_modal.php'; ?>
    <?php include '../common/add_modal.php'; ?>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>

    </body>


</html>
