<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #122738;border-color: #122738;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="<?=BASE_URI?>" style="color:#fff;position: absolute;font-size: 25px; font-weight: bold; vertical-align: middle; line-height: 40px; margin-left: 20px;text-decoration:none;">
        JYG<span class="text-danger" style="color: rgb(169, 68, 66); font-size: 7px; position: absolute; bottom: 9px;">ADMIN</span>
        <span style="display: block; margin-top: -24px; margin-left: 28px; font-weight: normal; font-size: 8px;"><small>TRAVEL & TOURS</small></span>
        </a>
    </div>

    <ul class="nav navbar-right top-nav">
        <li class="">
            <?php if(isset($_SESSION['name'])): ?>
            <a><?=$_SESSION['name']?></a>
            <?php endif;?>
        </li>
        <li class="">
            <a href="<?=BASE_URI?>logout.php">Logout</a>
        </li>
    </ul>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="<?=BASE_URI?>admin"><i class="fa fa-fw fa-dashboard"></i> Packages</a>
            </li>
            <li class="active">
                <a href="<?=BASE_URI?>admin/reservation.php"><i class="fa fa-fw fa-dashboard"></i> Reservation</a>
            </li>
            <li>
                <a href="charts.html"><i class="fa fa-fw fa-bar-chart-o"></i> Charts</a>
            </li>
        </ul>
    </div>



</nav>