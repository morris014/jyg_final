<div id="update_modal" class="modal  modal-flow fade">
  <div class="modal-dialog modal-sm" style="width: 35%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-size:13px;">Add New User</h4>
      </div>
      <div class="modal-body" style="padding:20px 15px;">
        <div class="row">
        <form method="post">
          <input type="hidden" name="package_id">
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input class="form-control" placeholder="Place" name="place" required>
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input class="form-control" placeholder="Title" name="title" required>
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input class="form-control" placeholder="Description" name="description" required>
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input type="number" class="form-control" placeholder="Price Per Head" name="price_per_head" required>
          </div>

          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <pre><textarea class="form-control" placeholder="Tour Details" name="tour_details" required></textarea></pre>
          </div>



          <div class="text-center col-sm-12" style="margin-top:25px;">
            <button type="submit" name="update_package" class="btn btnf-f btn-success btn-block">SAVE</button>
          </div>
        </form>          
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

