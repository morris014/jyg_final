<div id="delete_modal" class="modal  modal-flow fade">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-size:13px;">Are you sure you want to delete this?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-sm-12">
          <small>Note: You can't undo this action.</small>
        </div>
        <form method="post">
          <input type="hidden" name="package_id" value="<?=$rows['package_id']?>"/>
          <div class="text-center col-sm-12" style="margin-top:25px;">
            <button type="submit" name="delete_package" value="delete_package" class="btn btnf-f btn-danger btn-block">DELETE</button>
          </div>
        </form>          
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->