<head>
    
    <title>JYG TRAVEL AND TOURS</title>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/animate.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/app.css">
    <script src="<?=BASE_URI?>js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    <link rel="stylesheet" href="<?=BASE_URI?>vendor/scheduler/codebase/dhtmlxscheduler.css" type="text/css">
    <script src="<?=BASE_URI?>vendor/scheduler/codebase/dhtmlxscheduler.js" type="text/javascript"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script> 
    <!--<script src="<?=BASE_URI?>vendor/scheduler/codebase/ext/dhtmlxscheduler_map_view.js" type="text/javascript"></script>-->
        


</head>

