<style type="text/css">
    .notif-menu{
        position: : relative;
        width : 700px !important;
        margin-right: 900px !important;
    }

</style>
<div class="site-header">
    <div class="container">
        <div class="main-header">
            <div class="row">
                <div class="col-md-2 col-sm-6 col-xs-10">
                    <div class="logo">
                        <a href="#" style="font-size: 25px; font-weight: bold; vertical-align: middle; line-height: 40px; margin-left: 20px;">
                        JYG
                        <span style="display: block; margin-top: -24px; margin-left: 28px; font-weight: normal; font-size: 8px;"><small>TRAVEL & TOURS</small></span>
                            <!-- <img src="images/jyg.png" alt="travel html5 template" title="travel html5 template"> -->
                        </a>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->
                <div class="col-md-10 col-sm-6 col-xs-2">
                    <div class="main-menu">
                        <ul class="visible-lg visible-md">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="<?=BASE_URI?>blog.php">Travel Blog</a></li>
                            <li><a href="<?=BASE_URI?>promo.php">Travel Promo</a></li>
                            <li><a href="<?=BASE_URI?>about.php">About Us</a></li>
                            <li><a href="<?=BASE_URI?>contact.php">Contact</a></li>
                            <?php if(!isset($_SESSION['email'])): ?>
                            <li><a href="<?=BASE_URI?>login.php">Login</a></li>
                            <?php else: ?>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?=$_SESSION['name']?><b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <?php $user_type = isset($_SESSION['user_type']) ? $_SESSION['user_type'] : 0; ?>
                                        <?php if($user_type == 2): ?>
                                            <li><a href="<?=BASE_URI?>user/home.php">My Profile</a></li>
                                        <?php else : ?>
                                            <li><a href="<?=BASE_URI?>admin">My Profile</a></li>
                                        <?php endif; ?>
                                        <li class="divider"></li>
                                        <li><a href="<?=BASE_URI?>logout.php">Logout</a></li>
                                    </ul>
                              </li>
                            <?php endif;?>
           
                        </ul>
                        <a href="#" class="toggle-menu visible-sm visible-xs">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div> <!-- /.main-menu -->
                </div> <!-- /.col-md-8 -->
            </div> <!-- /.row -->
        </div> <!-- /.main-header -->

    </div> <!-- /.container -->
</div> <!-- /.site-header -->
