<div class="site-footer" style="margin-top:80px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="copyright">
                    <span>Copyright &copy; 2015 <a href="#">JYG Travels & Tours</a>
                </div>
            </div> <!-- /.col-md-4 -->
            <div class="col-sm-6">
                <ul class="social-icons">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-linkedin"></a></li>
                    <li><a href="#" class="fa fa-flickr"></a></li>
                    <li><a href="#" class="fa fa-rss"></a></li>
                </ul>
            </div> <!-- /.col-md-4 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.site-footer -->