<div id="form_modal" class="modal  modal-flow fade">
  <div class="modal-dialog modal-sm" style="width: 35%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="font-size:13px;">Confirm Reservation</h4>
      </div>
      <div class="modal-body" style="padding:20px 15px;">
        <div class="row">
        <form>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input type="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <div class="col-sm-5" style="padding:0 2px 0 0;">
              <input type="text"  class="form-control" placeholder="Firstname">
            </div>
            <div class="col-sm-5" style="padding:0 2px;">
              <input type="text" class="form-control" placeholder="Lastname">
            </div>
            <div class="col-sm-2" style="padding:0 0 0 2px;">
              <input type="text" class="form-control" placeholder="M.I">
            </div>
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input type="text" class="form-control" placeholder="Address">
          </div>
          <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
            <input type="text" class="form-control" placeholder="Contact Number">
          </div>

          <div class="text-center col-sm-12" style="margin-top:25px;">
            <button class="btn btnf-f btn-success btn-block">CONTINUE</button>
          </div>
        </form>          
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->