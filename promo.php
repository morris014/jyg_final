<?php include_once 'config.php'; ?>
<?php   $packages = mysqli_query($MySQLiconn,"SELECT * FROM packages ORDER BY RAND()") or  die($MySQLiconn->error); ?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <?php include 'common/head.php'; ?>
    <body>

    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <?php include 'common/header.php'; ?>
    <div class="flexslider">
        <ul class="slides">
            <li>
                <div class="overlay"></div>
                <img src="images/carousel/me-banaue-sagada-51.jpg" alt="Special 2">
            </li>
            <li>
                <div class="overlay"></div>
                <img src="images/carousel/Camiguin.jpg" alt="Special 3">
            </li>
        </ul>
    </div>

        <div class="container">
            <div class="row" style="margin-top: -400px;">
                <div class="our-listing owl-carousel">
                <?php while($row = mysqli_fetch_array($packages)): ?>
                    <div class="list-item">
                        <div class="list-thumb">
                            <div class="title">
                                <h4><?=$row['place']?></h4>
                            </div>
                            <img src="uploads/<?=$row['filename']?>" alt="destination 1">
                        </div> <!-- /.list-thumb -->
                        <div class="list-content">
                            <h5><?=$row['title']?></h5>
                            <span><?=$row['description']?></span>
                            <a href="book.php?package=<?=$row['package_id']?>" class="price-btn">Book Now</a>
                        </div> <!-- /.list-content -->
                    </div> <!-- /.list-item -->
                <?php endwhile; ?>
                </div> <!-- /.our-listing -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->

        <?php include 'common/footer.php'; ?>

    <script src="js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

    </body>


</html>