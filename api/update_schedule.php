

<?php
	include_once '../config.php';

	$user_id = $_SESSION['user_id'];
	

	if(!$user_id || $user_id <= 0){
		http_response_code(401);
	}

	
	$data = array(
		'user_id' => $user_id,
		'text' =>  $MySQLiconn->real_escape_string($_POST['text']),
		'event_location' => $_POST['event_location'],
		'start_date' => date_format(date_create($_POST['start_date']),'Y-m-d H:i:s'),
		'end_date' => date_format(date_create($_POST['end_date']),'Y-m-d H:i:s'),
		'event_id' => $_POST['event_id']
	);
	
    $MySQLiconn->begin_transaction();
    $SQL = $MySQLiconn->query("UPDATE schedules SET user_id = {$user_id},`text` = '{$data['text']}',event_location = '{$data['event_location']}',start_date = '{$data['start_date']}',end_date = '{$data['end_date']}' WHERE event_id = {$data['event_id']}");
    if(!$SQL){
    	http_response_code(400);
    	echo json_encode(array("err"=>$MySQLiconn->error));
    	die;
    } 

    $MySQLiconn->commit();
	http_response_code(200);
	echo json_encode(array("data"=>$data));
