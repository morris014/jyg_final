

<?php
	include_once '../config.php';


	$files = array();


	$user_id = $_SESSION['user_id'];

	$profile_pic = (!isset($_SESSION['profile_pic'])) ? null : $_SESSION['profile_pic'];

	if(!$user_id || $user_id <= 0){
		http_response_code(401);
	}


	$content  = nl2br($_POST['content']);
	$file_name = '';
	$date_posted = date('Y-m-d h:i:s A');
	

	if(isset($_GET['files'])){
		$error = false;
		$uploaddir = '../uploads/posts/';
	    foreach($_FILES as $file)
	    {
	    	
	    
	    	$filename = md5(uniqid($file['name'], true)).$file['name'];

	        if(move_uploaded_file($file['tmp_name'], $uploaddir .$filename))
	        {
	        	
	            $files[] = $uploaddir .$file['name'];
	        }
	        else
	        {
	            $error = true;
	        }
	    }
	}


	$data = array(
		'user_id' => $user_id,
		'filename' => UPLOADS_URI.$filename,
		'content' => $content,
		'date_posted' => $date_posted,
		'name' => $_SESSION['name'],
		'profile_pic' => getProfilePic($profile_pic)
	);
	
    $MySQLiconn->begin_transaction();
    $SQL = $MySQLiconn->query("INSERT INTO feeds(user_id,filename,content,date_posted) VALUES({$user_id},'{$filename}','{$content}','{$date_posted}')");
    if(!$SQL){
    	http_response_code(400);
    	echo json_encode(array("err"=>$MySQLiconn->error));
    	die;
    } 
    $data['feeds_id'] = $MySQLiconn->insert_id;

    $MySQLiconn->commit();


	http_response_code(200);
	echo json_encode(array("data"=>$data));
