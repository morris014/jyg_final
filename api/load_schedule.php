

<?php
	include_once '../config.php';

	$user_id = (!isset($_SESSION['user_id'])) ? null : $_SESSION['user_id'] ;
	if(!$user_id || $user_id <= 0 || $user_id == null){
		http_response_code(401);
		die;
	}

	$schedules = mysqli_query($MySQLiconn,"SELECT * FROM schedules WHERE user_id = {$user_id}") or  http_response_code(500); 
	
	$data = array();
	while ($row = mysqli_fetch_array($schedules)) {
		$data[] = ['text'=>$row['text'],'start_date'=>date_format(date_create($row['start_date']),'m/d/Y h:i'), 
				    'end_date'=>date_format(date_create($row['end_date']),'m/d/Y h:i'),'id'=>$row['event_id'],'event_location'=>$row['event_location']
				  ];
	}

    
	http_response_code(200);
	echo json_encode($data);