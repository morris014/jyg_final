

<?php
	include_once '../config.php';

	$user_id = $_SESSION['user_id'];
	$profile_pic = (!isset($_SESSION['profile_pic'])) ? null : $_SESSION['profile_pic'];


	if(!$user_id || $user_id <= 0){
		http_response_code(401);
		echo json_encode(array("err"=>"Unauthorized"));
		die;

	}

	$content  = nl2br($_POST['content']);
	$profile_pic = getProfilePic($profile_pic);
	$name = $_SESSION['name'];
	$date_posted = date('Y-m-d H:i:s');

	$feeds_id = $_POST['feeds_id'];

	$data = array(
		'user_id' => $user_id,
		'feeds_id' => $_POST['feeds_id'],
		'content' => $content,
		'date_posted' => date_format(date_create($date_posted),'M d, Y h:i:s A'),
		'profile_pic' => $profile_pic,
		'name' => $name
	);
	
    $MySQLiconn->begin_transaction();
    $SQL = $MySQLiconn->query("INSERT INTO feeds_comments(user_id,feeds_id,content,date_posted) VALUES({$user_id},{$feeds_id},'{$content}','{$date_posted}')");
    if(!$SQL){
    	http_response_code(400);
    	echo json_encode(array("err"=>$MySQLiconn->error));
    	die;
    } 

    $MySQLiconn->commit();
	http_response_code(200);
	echo json_encode(array("data"=>$data));
