

<?php include_once '../config.php'; ?>
<?php
  $success = -1;

  if(!$_SESSION['user_id'] || $_SESSION['user_id'] <= 0){
     header('Location: '. BASE_URI);
  }

  $user_id = $_SESSION['user_id'];

    if(isset($_POST['save'])){
    $id = $_POST['id'];
    $error = false;
    $uploaddir = '../uploads/';
    $filename = '';

    foreach($_FILES as $file)
    {
      
      $filename = md5(uniqid($file['name'], true)).$file['name'];

        if(move_uploaded_file($file['tmp_name'], $uploaddir .$filename))
        {
          
            $files[] = $uploaddir .$file['name'];
        }
        else
        {
            $error = true;
        }
    }
    $profile_pic = $filename;
    $name = $_POST['name'];
    $password = $_POST['password'];
    $contact_no = $_POST['contact_no'];
    $birthdate = $_POST['birthdate'];
    
    $_SESSION['name'] = $name;
  
    if(!empty($profile_pic) && file_exists($uploaddir.$profile_pic)){
      $query = "UPDATE user_account SET birthdate='{$birthdate}',name = '{$name}',password = '{$password}',profile_pic ='{$profile_pic}',contact_no='{$contact_no}' WHERE id = {$id}";  
    }else{
      $query = "UPDATE user_account SET birthdate='{$birthdate}',name = '{$name}',password = '{$password}',contact_no='{$contact_no}' WHERE id = {$id}";  
    }
    
    $result = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 
    if($result){ 
      echo "<script>alert('Saved Succesfully'); window.location.replace('edit.php');</script>";
      
      //header('Location: '.$_SERVER['HTTP_REFERER']);
    }
    
  }
  $query = "SELECT * FROM user_account where id = {$user_id} LIMIT 1";
  $user_account = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 
  $ua = mysqli_fetch_assoc($user_account);


?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/app.css">
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <style type="text/css">
body {
    font-family: Verdana;

}

/* USER PROFILE STYLES */

    .user-wrapper .btn {
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        margin: 5px;
    }

    .user-wrapper .description {
        padding: 25px;
        
    }
    .user-wrapper img {
        width:100%;
    }
    #filePhoto{
        display: none;
    }

    #feedContainer .row{
       padding: 0px 0px 20px 0px;
    }

    pre {
       background-color: white;
    }
    .name-link{
      font-size:14px;font-family: Tahoma, Verdana, Segoe, sans-serif;color:#3385ff
    }
</style>
    <div id="wrapper">
    <?php include 'user_header.php'; ?>

        <div id="page-wrapper">
            <div class="contact-page">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-1 wall-container">

                            <div class="user-wrapper">
                              <?php
                                $profpic = getProfilePic($ua['profile_pic']);
                                
                              ?>
                              <img id="previewHolder" style="height:200px;width:200px" src="<?=$profpic?>" class="img-responsive img-circle" /> 

                              <form method="post" enctype="multipart/form-data">
                              <input type="hidden" name="id" value="<?=$ua['id']?>">
                              <input style="display:none" id="changeProfilePic" name="profile_pic" type="file" value="<?=$profpic?>">
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <button type="button" id="btnChangeProfilePic">Change Profie Picture</button>
                              </div>
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <input class="form-control" placeholder="Name" name="name" required value="<?=$ua['name']?>">
                              </div>
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <input type="password" class="form-control" placeholder="Password" name="password" required value="<?=$ua['password']?>">
                              </div>
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <input class="form-control" placeholder="Contact No" name="contact_no" required value="<?=$ua['contact_no']?>">
                              </div>
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <input type="date" class="form-control" placeholder="Birthdate" name="birthdate" required value="<?=$ua['birthdate']?>">
                              </div>
                              <div class="form-group" style="padding:5px 18px;margin-bottom:0;">
                                <button name="save" type="submit" class="btn-primary">Save</button>
                              </div>
                            </form>          
                      
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){

           var selected_file = null;

            function readURL(input) {
              if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $('#previewHolder').attr('src', e.target.result).show();  
                }
                reader.readAsDataURL(input.files[0]);
                selected_file = input.files[0];
              }
            }

            $("#changeProfilePic").change(function() {
              readURL(this);
            });

          $('#btnChangeProfilePic').click(function(){
              $('#changeProfilePic').click();
          });
      });


    </script>
    </body>



</html>

