<?php include_once '../config.php'; ?>



<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/app.css">
    <link rel="stylesheet" href="<?=BASE_URI?>vendor/scheduler/codebase/dhtmlxscheduler.css" type="text/css">
    <script src="<?=BASE_URI?>vendor/scheduler/codebase/dhtmlxscheduler.js" type="text/javascript"></script>
    <script src="<?=BASE_URI?>vendor/scheduler/codebase/ext/dhtmlxscheduler_map_view.js" type="text/javascript"></script>
        
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <style type="text/css">
body {
    font-family: Verdana;

}

/* USER PROFILE STYLES */

    .user-wrapper .btn {
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        margin: 5px;
    }

    .user-wrapper .description {
        padding: 25px;
        
    }
    .user-wrapper img {
        width:100%;
    }
    #filePhoto{
        display: none;
    }

    #feedContainer .row{
       padding: 0px 0px 20px 0px;
    }

    pre {
       background-color: white;
    }
    .name-link{
      font-size:14px;font-family: Tahoma, Verdana, Segoe, sans-serif;color:#3385ff
    }
</style>
    <div id="wrapper">
    <?php include 'user_header.php'; ?>
        <div id="page-wrapper">
          <div class="contact-page">
              <div class="col-md-12 col-sm-6">
                <div id="scheduler_here" class="dhx_cal_container" style='left:50px;width:100%; height:700px;'>
                  <div class="dhx_cal_navline">
                  <div class="dhx_cal_prev_button">&nbsp;</div>
                  <div class="dhx_cal_next_button">&nbsp;</div>
                    <!-- <div class="dhx_cal_today_button"></div> -->
                  <div class="dhx_cal_date"></div>
                                <!-- <div class="dhx_cal_tab" name="month_tab"></div>
                                <div class="dhx_cal_tab" name="map_tab" style="right:280px;"></div> -->
                </div>
                <div class="dhx_cal_header"></div>
                <div class="dhx_cal_data"></div>       
            </div>
                    </div>
          </div>
        </div>  
    </div>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>
            <script type="text/javascript">
        function createEvent(data){
            return $.ajax({
                url: '../api/add_schedule.php',
                data: data,
                type: 'post',
                dataType: 'json'
            });
        }
        function updateEvent(data){
            return $.ajax({
                url: '../api/update_schedule.php',
                data: data,
                type: 'post',
                dataType: 'json'
            });
        }
        function getShedules(){
            return $.ajax({
                url : '../api/load_schedule.php',
                type: 'get',
                dataType: 'json'
            });
        }
        function init(){
            
            scheduler.init('scheduler_here', new Date(),"month");
            
            getShedules().done(function(res){
                scheduler.parse(res,'json');

            }).fail(function(xhr){
                console.log(xhr);
                if(xhr.status == 401) return alert('Please login to view schedules');
                /*alert('Failed to load schedules');*/
            });



            //scheduler.locale.labels.map_tab = "Map";
            scheduler.config.lightbox.sections=[    
                {name:"description", height:50,map_to:"text", type:"textarea", focus:true},
                {name:"location", height:43, map_to:"event_location", type:"textarea"},
                {name:"reminders", height:50,map_to:"reminders", type:"textarea", focus:true},    
                {name:"time", height:72, type:"time", map_to:"auto"},

            ]
            scheduler.locale.labels.section_location = "Location";
            

            scheduler.attachEvent("onEventSave",function(id,e,is_new){
                //console.log('DATA',e.start_date.toLocaleString());

                var data = {
                    event_id : id,
                    start_date : e.start_date.toLocaleString(),
                    end_date   : e.end_date.toLocaleString(),
                    text : e.text,
                    event_location : e.event_location
                }
                 
                
                if (!e.text) {
                    alert("Text must not be empty");
                    return false;
                }
                /*if (e.text.length<20) {
                    alert("Text too small");
                    return false;
                }*/
                if(is_new){
                    

                    createEvent(data).done(function(res){
                        console.log(res);   
                        alert('Saved Succesfully');
                        return true;
                    }).fail( function(xhr, textStatus, errorThrown) {
                        if(xhr.status == 401){
                            alert('Please login to continue');
                        
                        }
                        alert('Saving failed. Please try again later');
                        return false;
                    });
                }else{
                    
                    updateEvent(data).done(function(res){
                        console.log(res);   
                        alert('Saved Succesfully');
                        return true;
                    }).fail( function(xhr, textStatus, errorThrown) {
                        if(xhr.status == 401){
                            alert('Please login to continue');
                        
                        }
                        alert('Saving failed. Please try again later');
                        return false;
                    });                    
                }
              return true;
            })
        }
          
        $(function(){
            $(document).ready(function(){
                init();
            });
        });
        </script>
    </body>


</html>
