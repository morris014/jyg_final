<?php include_once '../config.php'; ?>
<?php
  if(!$_SESSION['user_id'] || $_SESSION['user_id'] <= 0){
     header('Location: '. BASE_URI);
    
  }
  $user = $_SESSION['user_id'];

  $feeds_id = $_GET['feedsId'];

  if(!$feeds_id){
      $query = "SELECT f.id as feed_id,f.filename,f.content,ua.name,f.date_posted,ua.profile_pic FROM feeds f 
            LEFT JOIN user_account ua ON f.user_id = ua.id
            WHERE ua.id = '$user'
            ORDER BY date_posted DESC";  
  }else{
      $query = "SELECT f.id as feed_id,f.filename,f.content,ua.name,f.date_posted,ua.profile_pic FROM feeds f 
            LEFT JOIN user_account ua ON f.user_id = ua.id
            WHERE f.id = {$feeds_id}
            ORDER BY date_posted DESC";  
  }
  

  $feeds = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 

  function getFeedsComments($feeds_id,$MySQLiconn){

      $query = "SELECT fc.content,fc.date_posted,ua.name,ua.profile_pic FROM feeds_comments fc
                INNER JOIN user_account ua ON fc.user_id = ua.id
                WHERE fc.feeds_id = {$feeds_id}
                ORDER BY fc.date_posted DESC

                ";
      $comments = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 
      return $comments;
  }
?>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> 
    <html class="no-js"> 
    <!--<![endif]-->
    <link rel="stylesheet" href="<?=BASE_URI?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/font-awesome.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/admin.css">
    <link rel="stylesheet" href="<?=BASE_URI?>css/app.css">
    <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->
    <style type="text/css">
body {
    font-family: Verdana;

}

/* USER PROFILE STYLES */

    .user-wrapper .btn {
        border-radius: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        margin: 5px;
    }

    .user-wrapper .description {
        padding: 25px;
        
    }
    .user-wrapper img {
        width:100%;
    }
    #filePhoto{
        display: none;
    }

    #feedContainer .row{
       padding: 0px 0px 20px 0px;
    }

    pre {
       background-color: white;
    }
    .name-link{
      font-size:14px;font-family: Tahoma, Verdana, Segoe, sans-serif;color:#3385ff
    }
</style>
    <div id="wrapper">
    <?php include 'user_header.php'; ?>

        <div id="page-wrapper">
<div class="contact-page">
            <div class="container">
                <div class="row">
                     <!-- <div class="col-md-3 col-sm-6 map-wrapper">
                          <div class="user-wrapper">
                        <img src="http://packetcode.com/apps/wall-design/image.jpg" class="img-responsive img-circle" /> 
                    <div class="description">
                       <h4> Ramolin Leomarid</h4>
                        <h5> <strong> Website Designer </strong></h5>
                        <p>
                            Pellentesque elementum dapibus convallis. 
                        </p>
                        <hr />
                        <a href="#" class="btn btn-danger btn-sm"> <i class="fa fa-user-plus" ></i> &nbsp;Profile + </a> 
                    </div>
                     </div>
                    </div>  -->
                    <div class="col-md-9 col-sm-6 col-md-offset-1 wall-container">
                        <div <?php if($feeds_id) echo "hidden"; ?>>
                            <div class="row">
                            <form method="post" id="frmPost" >
                              <div class="col-md-10 col-md-push-2"><textarea placheholder="Share your experiences with others" class="form-control" id="feedbox" rows="2"></textarea><br>
                              <div id="photoContainer">
                                  <a id="close" href="#"></a>
                                  <input type="file" name="filePhoto" value="" id="filePhoto" class="required borrowerImageFile" data-errormsg="PhotoUploadErrorMsg">
                                  <img hidden id="previewHolder" alt="Uploaded Image Preview Holder" width="100%" height="250px"/>
                              </div>

                              <button hidden style="margin-left:10px" type="button" id="btnCancel" class="btn btn-default pull-right">Cancel</button>
                              <button type="submit" id="button" class="btn btn-default pull-right">Post</button>
                              <button style="margin-right:10px" type="button" id="btnAddPhotos" class="btn btn-default pull-right">Add Photos</button>

                              </div>
                            </form>
                            </div>
                        </div>
                        <hr>
                            <div id="insert"></div>
                        <div id="feedContainer">

                        <?php while($row = mysqli_fetch_array($feeds)): ?>
                            <?php 
                                $comments = getFeedsComments($row['feed_id'],$MySQLiconn); 
                                $profile_pic = getProfilePic($row['profile_pic']);
                            ?>
                            <div class="row" id="feed" >
                              <div class="col-md-2"><img src="<?=$profile_pic?>" class="img-circle" width="100%"/></div>
                              <div class="col-md-10" style="border:1px solid #d3d3d3;">
                                  <div><b><a href="" class="name-link" style=""><?=$row['name']?></a></b>

                                  <div class="pull-right text-muted" id="delete"></div></div>
                                  <div>
                                      <p style="padding: 10px 0px 30px 0px;font-family: Tahoma, Verdana, Segoe, sans-serif;color:black"><?=$row['content'] ?></p>
                                      <?php if($row['filename'] !== ''): ?>
                                        <img style="width:100%;height:200px" src="<?=UPLOADS_URI.$row['filename']?>">
                                      <?php endif;?>
                                  </div>
                                  <div class="text-muted"> <small>posted: <?=date_format(date_create($row['date_posted']),'M d, Y h:i:s A')?></small></div>
                                  <div class="text-muted"><a <?php if($feeds_id) echo "hidden"; ?> href="#" class="view-comments">Comments : <?=$comments->num_rows;?></a></div>
                                  <div <?php if(!$feeds_id) echo "hidden"; ?> id="commentsContainer" style="margin-top:10px">
 
                                     <?php 
                                        while($comment = mysqli_fetch_array($comments)): 
                                        $profile_pic = getProfilePic($comment['profile_pic']);
                                      ?>

                                        <div class="media">
                                              <a class="pull-left" href="#">
                                                  <img style="height:50px;width:50px" class="media-object" src="<?=$profile_pic?>" alt="">
                                              </a>
                                              <div class="media-body">
                                                  <h4 class="media-heading name-link"><?=ucwords($comment['name'])?></h4>
                                                  <pre><?=$comment['content']?></pre>
                                                  <small><?=date_format(date_create($comment['date_posted']),'M d , Y h:i:s A')?></small>
                                              </div>
                                        </div>
                                     <?php endwhile;?>
                                  </div>

                                  <div style = "margin-top:10px">
                                        <form feeds-id="<?=$row['feed_id']?>" role="form" class="frmAddComment">
                                            <div class="form-group">
                                                <textarea rows="1" class="form-control add-comment" placeholder="Write your comments here"></textarea>
                                            </div>
                                        </form>
                                 </div>
                              </div>
                            </div>
                            
                        <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         </div>

    </div>

    <script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=BASE_URI?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
    <script src="<?=BASE_URI?>js/bootstrap.js"></script>
    <script src="<?=BASE_URI?>js/plugins.js"></script>
    <script src="<?=BASE_URI?>js/main.js"></script>
    <script type="text/javascript" src="<?=BASE_URI?>js/myprofile.js"></script>
    </body>


</html>
