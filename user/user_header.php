<?php
    

    function getReminders($MySQLiconn){
        $user_id = $_SESSION['user_id'];
        $query = "SELECT COUNT(*) AS total_schedules FROM schedules WHERE start_date BETWEEN NOW() AND DATE_ADD(NOW(),INTERVAL 14 DAY) AND user_id = {$user_id}";
        $reminders = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error); 
        return $reminders;
    }

    function getCommentors($feeds_id,$MySQLiconn){
        $user_id = $_SESSION['user_id'];
        $query = "SELECT COUNT(*) as total,ua.name
                FROM feeds_comments fc 
                INNER JOIN user_account ua ON ua.id = fc.user_id
                WHERE fc.feeds_id = {$feeds_id} AND fc.user_id != {$user_id} GROUP BY fc.user_id ORDER BY fc.date_posted DESC LIMIT 3
                ";

        $commentors = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error);
        $data = array();
        $total = 0;
        while($row = mysqli_fetch_array($commentors)){
            $data[] = $row['name'];
            $total = $row['total'];
        }
        $others = $total - sizeof($data);

        if(sizeof($data) == 1){
            return implode(',', $data);
        }else if(sizeof($data) == 2){
            return implode(' and ', $data);
        }else if($others > 0){
            return implode(',', $data).' and '. $total .' others ';    
        }else
        {
            return implode(',', array_slice($data, 0, sizeof($data) - 1)).' and '.$data[sizeof($data) - 1];
        }
        
        
    }
    function getFeedsNotif($MySQLiconn){
        $user_id = $_SESSION['user_id'];

        $query = "SELECT 
                n.id,n.feeds_id,
                n.date_updated,n.receiver_id 
                FROM notification n WHERE n.receiver_id = {$user_id}";

        $notif = mysqli_query($MySQLiconn, $query) or  die($MySQLiconn->error);
        return $notif;
    }
?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color: #122738;border-color: #122738;">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="<?=BASE_URI?>" style="color:#fff;position: absolute;font-size: 25px; font-weight: bold; vertical-align: middle; line-height: 40px; margin-left: 20px;text-decoration:none;">
        JYG<span class="text-danger" style="color: rgb(169, 68, 66); font-size: 7px; position: absolute; bottom: 9px;">User Profile</span>
        <span style="display: block; margin-top: -24px; margin-left: 28px; font-weight: normal; font-size: 8px;"><small>TRAVEL & TOURS</small></span>
        </a>
    </div>

    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
            <ul style="width:400px !important" class="dropdown-menu alert-dropdown">
                <?php
                    $reminders = mysqli_fetch_assoc(getReminders($MySQLiconn));
                    $feeds_notif = getFeedsNotif($MySQLiconn);
                    $has_notif = 0;
                ?>
                <?php if($reminders['total_schedules'] > 0): ?>
                    <?php $has_notif = 1; ?>
                    <li>
                        <a href="calendar.php">
                            <span class="label label-primary">Schedule Reminders</span><br>
                            You have <?=$reminders['total_schedules']?> scheduled event within this week <br>
                            <div class="text-muted"> <small><small><?=date('M d,Y h:i:s A')?></small></small></div>
                        </a>
                    </li>
                <?php endif; ?>
                 <?php while($row = mysqli_fetch_array($feeds_notif)): ?>
                    <?php $has_notif = 1; ?>
                    <li>
                        <a href="home.php?feedsId=<?=$row['feeds_id']?>">
                            <span class="label label-primary">Comments</span><br>
                            <?=getCommentors($row['feeds_id'],$MySQLiconn)?> also commented on your post<br>
                            <div class="text-muted"> <small><small><?=date_format(date_create($row['date_updated']),'M d, Y h:i:s A')?></small></small></div>
                        </a>
                    </li>
                <?php endwhile; ?>
                <?php if($has_notif == 0): ?>
                    <li>
                        <a href="#">
                           No new notifications
                        </a>
                    </li>
                <?php endif;?>
                
                <li class="divider"></li>
                <li>
                    <a href="#">View All</a>
                </li>
            </ul>
        </li>
        <li class="">
            <?php if(isset($_SESSION['name'])): ?>
            <a><?=$_SESSION['name']?></a>
            <?php endif;?>
        </li>
        <li class="">
            <a href="<?=BASE_URI?>logout.php">Logout</a>
        </li>

    </ul>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="<?=BASE_URI?>user/home.php"><i class="fa fa-fw fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <a href="<?=BASE_URI?>user/calendar.php"><i class="fa fa-fw fa-dashboard"></i> Calendar</a>
            </li>
             
            <li class="active">
                <a href="<?=BASE_URI?>user/edit.php"><i class="fa fa-fw fa-dashboard"></i> Edit Profile</a>
            </li>
        </ul>
    </div>



</nav>