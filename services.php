<?php include_once 'config.php'; ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php include 'common/head.php'; ?>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <?php include 'common/header.php'; ?>

        
<div class="page-top" id="templatemo_contact">
</div> <!-- /.page-header -->


        <div class="middle-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="widget-item">
                            <h3 class="widget-title">Latest News</h3>
                            <div class="post-small">
                                <div class="post-date">
                                    <span class="time">18</span>
                                    <span>June</span>
                                </div> <!-- /.post-thumb -->
                                <div class="post-content">
                                    <h4><a href="#">Lorem Ipsum</a></h4>
                                    <span>Donec auctor iaculis</span>
                                    <p>Nunc aliquam vestibulum veliesat, et aliquet lorem aliquet nec. Sed orsum rhoncus ipsum imperdiet accumsan laoreient.</p>
                                </div> <!-- /.post-content -->
                            </div> <!-- /.post-small -->
                            <div class="post-small">
                                <div class="post-date">
                                    <span class="time">14</span>
                                    <span>June</span>
                                </div> <!-- /.post-thumb -->
                                <div class="post-content">
                                    <h4><a href="#">Lorem Ipsum</a></h4>
                                    <span>Donec auctor iaculis</span>
                                    <p>Nunc aliquam vestibulum veliesat, et aliquet lorem aliquet nec. Sed orsum rhoncus ipsum imperdiet accumsan laoreient.</p>
                                </div> <!-- /.post-content -->
                            </div> <!-- /.post-small -->
                            <div class="post-small">
                                <div class="post-date">
                                    <span class="time">12</span>
                                    <span>June</span>
                                </div> <!-- /.post-thumb -->
                                <div class="post-content">
                                    <h4><a href="#">Lorem Ipsum</a></h4>
                                    <span>Donec auctor iaculis</span>
                                    <p>Nunc aliquam vestibulum veliesat, et aliquet lorem aliquet nec. Sed orsum rhoncus ipsum imperdiet accumsan laoreient.</p>
                                </div> <!-- /.post-content -->
                            </div> <!-- /.post-small -->
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4">
                        <div class="widget-item">
                            <h3 class="widget-title">Consulting</h3>
                            <div class="sample-thumb">
                                <img src="images/event_1.jpg" alt="">
                            </div> <!-- /.sample-thumb -->
                            <h4 class="consult-title">Donec auctor iaculis libero ut ullamcorper</h4>
                            <p>Praesent ornare commodo tincidunt. Interdum et fames ac ante ipsum primis in faucibus. Aliquam justo lectus, fermentum vitae libero, tincidunt accumsan magns. <br><br>Vestibulum congue lorem odio, at sodales nisi luctus quis. Suspendisse suscipit ligula libero, id consectetur magna dictum sed.</p>
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4">
                        <div class="widget-item">
                            <h3 class="widget-title">Our Services</h3>
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-bell-o"></i>
                                </div> <!-- /.service-icon -->
                                <div class="service-content">
                                    <h4>Lorem Ipsum</h4>
                                    <p>Change icons by <a rel="nofollow" href="http://fontawesome.info/font-awesome-icon-world-map/">Font Awesome</a> (version 4). Example: <strong>&lt;i class=&quot;fa fa-bell-o&quot;&gt;&lt;/i&gt;</strong></p>
                                </div> <!-- /.service-content -->
                            </div> <!-- /.service-item -->
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-cogs"></i>
                                </div> <!-- /.service-icon -->
                                <div class="service-content">
                                    <h4>Lorem Ipsum</h4>
                                    <p>Nunc aliquam vestibulum veliesat, et aliquet lorem aliquet nec. Sed orsum rhoncus.</p>
                                </div> <!-- /.service-content -->
                            </div> <!-- /.service-item -->
                            <div class="service-item">
                                <div class="service-icon">
                                    <i class="fa fa-pencil"></i>
                                </div> <!-- /.service-icon -->
                                <div class="service-content">
                                    <h4>Lorem Ipsum</h4>
                                    <p>Nunc aliquam vestibulum veliesat, et aliquet lorem aliquet nec. Sed orsum rhoncus.</p>
                                </div> <!-- /.service-content -->
                            </div> <!-- /.service-item -->
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.middle-content -->







        <div class="ticket-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="ticket-item">
                            <h4><a href="#">Airlines</a></h4>
                            <p>Travel is free responsive HTML5 template from <span class="blue">template</span><span class="green">mo</span>. You may download, edit and use this template for your travel related websites.<br><br> Aliquam justo lectus, fermentum vitae libero sollicitudin, tincidunt accumsan magna.Vestibulum congue lorem odio, at sodales nisi luctus quis.</p>
                            <a href="#" class="ticket-btn">Book a Ticket</a>
                        </div> <!-- /.ticket-item -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="ticket-item">
                            <h4><a href="#">Best Cities</a></h4>
                            <p>Praesent ornare commodo tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam justo lectus, fermentum vitae libero sollicitudin, tincidunt accumsan magna. <br><br>Vestibulum congue lorem odio, at sodales nisi luctus quis. Suspendisse suscipit ligula libero, id consectetur magna dictum sed.</p>
                            <a href="#" class="ticket-btn">Book a Ticket</a>
                        </div> <!-- /.ticket-item -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="ticket-item">
                            <h4><a href="#">Travel Guides</a></h4>
                            <p>Praesent ornare commodo tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam justo lectus, fermentum vitae libero sollicitudin, tincidunt accumsan magna. <br><br>Vestibulum congue lorem odio, at sodales nisi luctus quis. Suspendisse suscipit ligula libero, id consectetur magna dictum sed.</p>
                            <a href="#" class="ticket-btn">Book a Ticket</a>
                        </div> <!-- /.ticket-item -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="ticket-item">
                            <h4><a href="#">Special Events</a></h4>
                            <p>Praesent ornare commodo tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam justo lectus, fermentum vitae libero sollicitudin, tincidunt accumsan magna. <br><br>Vestibulum congue lorem odio, at sodales nisi luctus quis. Suspendisse suscipit ligula libero, id consectetur magna dictum sed.</p>
                            <a href="#" class="ticket-btn">Book a Ticket</a>
                        </div> <!-- /.ticket-item -->
                    </div> <!-- /.col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.ticket-list -->


        <?php include 'common/footer.php'; ?>
        
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        
        <!-- Google Map -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        
        <!-- Google Map Init-->
        <script type="text/javascript">
            jQuery(function($){
                $('.first-map').gmap3({
                    marker:{
                        address: '16.8496189,96.1288854' 
                    },
                        map:{
                        options:{
                        zoom: 16,
                        scrollwheel: false,
                        streetViewControl : true
                        }
                    }
                });
            });
        </script>
        <!-- templatemo 409 travel -->
    </body>
</html>