<?php include_once 'config.php'; ?>

<?php
  

  $package_id = isset($_GET['package']) ? $_GET['package'] : 0;
  if($package_id <= 0) Header('Location: '.BASE_URI);

  $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

  $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
  if(!$_SESSION['user_id'] || $_SESSION['user_id'] <= 0){
     header('Location: '. BASE_URI.'login.php?redirect='.$escaped_url);
  }

  $query = mysqli_query($MySQLiconn,"SELECT * FROM packages WHERE package_id = {$package_id} LIMIT 1") or  die($MySQLiconn->error);
  $details = mysqli_fetch_assoc($query);

  $success = false;
  if($_SERVER['REQUEST_METHOD'] == "POST"){ 
            function getName($object){
              return $object['firstname'].' '.$object['lastname'];
            }
            $user_id = $_SESSION['user_id'];
            $package_id = $MySQLiconn->real_escape_string($_POST['package_id']);
            $first_name = $MySQLiconn->real_escape_string($_POST['first_name']);
            $last_name = $MySQLiconn->real_escape_string($_POST['last_name']);
            $address = $MySQLiconn->real_escape_string($_POST['address']);
            $contact_no = $MySQLiconn->real_escape_string($_POST['contact_no']);
            $no_of_adults = $MySQLiconn->real_escape_string($_POST['no_of_adults']);
            $no_of_infants = $MySQLiconn->real_escape_string($_POST['no_of_infants']);
            $email = $MySQLiconn->real_escape_string($_POST['email']);
            $adults_name = implode(',', array_map('getName',$_POST['adults']));
            $infants_name = implode(',', array_map('getName',$_POST['infants']));


            $MySQLiconn->begin_transaction();
            $SQL = $MySQLiconn->query("INSERT INTO customer_reservation(package_id,user_id,first_name,last_name,address,contact_no,no_of_adults,no_of_infants,adults_name,infants_name,email) VALUES({$package_id},{$user_id},'{$first_name}','{$last_name}','{$address}','{$contact_no}',{$no_of_adults},{$no_of_infants},'{$adults_name}','{$infants_name}','{$email}')");
            if(!$SQL){
                die($MySQLiconn->error);
            } 

            $MySQLiconn->commit();
            $success = true;
            $message = "You have reserved succesfully";
            echo "<script>alert('You have reserved succesfully');window.location.replace('book.php?package=".$package_id."')</script>";
    }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php include 'common/head.php'; ?>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <?php include 'common/header.php'; ?>

        
        <div class="page-top" id="templatemo_contact">
        </div> <!-- /.page-header -->

<style>
.height {
    min-height: 200px;
}

.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
</style>

<div class="site-content container">

    <div class="row">
        <div class="col-md-4" id="leftCol">
           <?php if($success): ?>
                <div class="alert alert-success">
                  <strong>Success!</strong><?=$message?>
                </div>
           <?php endif;?>
            <form id="frmBook" method="post" enctype="multipart/form-data">
              <input type="hidden" name="price_per_head" value="<?=$details['price_per_head']?>">
                <div class = "row">
                  <div class="col-sm-6">
                    <input type="hidden" name="package_id" value="<?=$package_id?>">
                    <div class="form-group" style="margin-bottom:0;">
                      <input class="form-control" placeholder="First Name" name="first_name" required>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group" style="margin-bottom:0;">
                          <input class="form-control" placeholder="Last Name" name="last_name" required>
                    </div>
                  </div>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                  <input type="email" class="form-control" placeholder="Address" name="email" required>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                  <textarea class="form-control" placeholder="Address" name="address" required></textarea>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                  <input class="form-control" placeholder="Contact No" name="contact_no" required>
                </div>
                
                <div class="form-group">
                  <b>Adults</b>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                  <input min = "0" id="adults" type="number" class="form-control" placeholder="No. of adults" name="no_of_adults" required>
                </div>
                <div id="adultsContainer">
     
                </div>
                <div class="form-group">
                  <b>Infants</b>
                </div>
                <div class="form-group" style="margin-bottom:0;">
                  <input min = "0" id="infants" type="number" class="form-control" placeholder="No. of infants" name="no_of_infants" required>
                </div>
                <div id="infantsContainer">
           
                </div>
        
              </form>          
              <div class="text-center col-sm-12" style="margin-top:25px;">
                <button id='btnBookNow' name="book_package" class="btn btnf-f btn-success btn-block">Book Now</button>
              </div>
         </div><!--/left-->

          <div class="col-md-8">
            <div class="intro col-sm-12">
              <div class="bookmark">
                <h1 id="sec0"><?=$details['place']?></h1>
                <h3 class="package-description" style="font-size: 13px; margin: 4px 0px 0;"><?=$details['description']?></h3>
                <p style="margin: 4px 0px 20px;"><label>PRICE:</label> <span><?=$details['price_per_head']?> per head</span></p>
                <p style="margin: 4px 0px 20px;"><label>PACKAGE INCLUSIONS</label></p>
                <?=$details['tour_details']?>
                <div class="row">
                  <div class="col-md-4"><img src="uploads/<?=$details['filename']?>" class="img-responsive"></div>
                </div>
              </div>
            </div><!--/right-->
          </div><!--/row-->
</div><!--/container-->
<!-- Modal -->
<div id="confirmModal" class="modal fade" role="dialog" style="margin-top:20px">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="text-center"><strong>Payment Details</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>

                                <tr>
                                    <td><strong>Type Of Guest</strong></td>
                                    <td class="text-center"><strong>No. of Guest</strong></td>
                                    <td class="text-center"><strong>Amount Per Guest</strong></td>
                                    <td class="text-right"><strong>Total</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Adults</td>
                                    <td id="adults_no_of_guest" class="text-center"></td>
                                    <td id="adults_amount_per_guest" class="text-center"><?=$details['price_per_head']?></td>
                                    <td id="adults_total" class="text-right"></td>
                                </tr>
                                <tr>
                                    <td>Infants</td>
                                    <td id="infants_no_of_guest" class="text-center"></td>
                                    <td id="infants_amounts_per_guest" class="text-center"><?=$details['price_per_head']?></td>
                                    <td id="infants_total" class="text-right"></td>
                                </tr>
                                
                                <tr>
                                    <td class="highrow"></td>
                                    <td class="highrow"></td>
                                    <td class="highrow text-center"><strong>Subtotal</strong></td>
                                    <td id="subtotal"class="highrow text-right">0</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow">
                                      <p>
                                        *** RATES ARE MAY CHANGE WITHOUT PRIOR NOTICE *** <br>
                                        <b>For Reservation:</b> <br>
                                        60% minimum downpayment required for the reservation of booking and <br>
                                        full payment must be made 2 weeks before the flight. Once the downpayment is received <br>
                                        Ticket itinerary and Confirmation Vouches will be sent to your email <br>
                                        <br><br>
                                        <b>Mode of payment</b> : CASH BASIS ONLY <br>
                                        <b>Bank</b> : BDO <br>
                                        <b>Name</b> : Marla N. Paraguan <br>
                                        <b>Account Number</b>  : 00490064506 <br>
                                        <b>Branch</b> : Airport MD Pasay City
                                      </p>
                                    </td>
                                    <td class="emptyrow"></td>
                                    <td id="discount" class="emptyrow text-center"><strong>Discount</strong></td>
                                    <td class="emptyrow text-right">0</td>
                                </tr>
                                <tr>
                                    <td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
                                    <td class="emptyrow"></td>
                                    <td  class="emptyrow text-center"><strong>Total</strong></td>
                                    <td id="total" class="emptyrow text-right">0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel-footer">
                    <button id="btnConfirm" type="button" class="btn btn-primary">Confirm </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>


</div>

        <?php include 'common/footer.php'; ?>
        
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/book.js"></script>
        <script type='text/javascript'>

        $(document).ready(function() {
        /* activate sidebar */
          $('#sidebar').affix({
            offset: {
            top: 235
            }
          });

          var $body   = $(document.body);
          var navHeight = $('.navbar').outerHeight(true) + 10;

          $body.scrollspy({
            target: '#leftCol',
            offset: navHeight
          });

          $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
              if (target.length) {
                $('html,body').animate({
                scrollTop: target.offset().top - 50
                }, 1000);
                return false;
              }
            }
          });

        });

        </script>


    </body>
</html>