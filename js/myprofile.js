           function addComments(content,feeds_id){
                return $.ajax({
                    url : '../api/add_comment.php',
                    type: 'post',
                    data: {content: content,feeds_id : feeds_id},
                    dataType: 'json'
                });
            }

            function addPosts(data){
                  return $.ajax({
                            url : '../api/post.php?files',
                            type: 'post',
                            data: data,
                            processData: false,
                            contentType: false,
                            dataType: 'json'
                        });
            }
            $(function(){
                $(document).on('keydown','.add-comment',function(e){
                    if(e.keyCode == 13 && !e.shiftKey){
                        e.preventDefault();

                        $(this).closest('form').submit();
                    }
                });
                $(document).on('submit','.frmAddComment',function(e){
                    e.preventDefault();     
                    var content = $(this).find('.add-comment').val();
                    var feeds_id = $(this).attr('feeds-id');                      
                    if(content == '' || !content){ return false;}


                    var commentsContainer = $(this).parent().parent().find('#commentsContainer');
                    var viewCommentsContainer = $(this).parent().parent().find('.view-comments');
                    var addCommentsContainer = $(this).find('.add-comment');

                    addComments(content,feeds_id).done(function(res){
                        console.log(res);
                        commentsContainer.show();
                        viewCommentsContainer.hide();
                        addCommentsContainer.val('');

                        var data = res.data;

                        var html = '<div class="media">';
                            html +=   '<a class="pull-left" href="#">';
                            html +=     '<img style="height:50px;width:50px" class="media-object" src="'+ data.profile_pic + '" alt="">';
                            html +=   '</a>';
                            html +=   '<div class="media-body">';
                            html +=       '<h4 class="media-heading name-link"> ' + data.name + '</h4>';
                            html +=       '<pre>' + content + '</pre>';
                            html +=       '<small>  '+ data.date_posted +'</small>';
                            html +=   '</div>';
                            html += '</div>';

                        $(html).prependTo(commentsContainer).hide().fadeIn();
                    }).fail(function(err,res,body){
                        console.log(err);
                        if(err.status == 401){
                            alert('Please login first');
                        }
                        
                    });                    
                });

                $(document).on('click','.view-comments',function(e){
                    e.preventDefault();
                    $(this).hide();
                    console.logs($(this).parent().parent().find('#commentsContainer').show());

                }); 
                $(document).ready(function(){
                  
                    var selected_file = null;

                    function readURL(input) {
                      if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function(e) {
                          $('#previewHolder').attr('src', e.target.result).show();  
                          $('#btnCancel').show();
                        }

                        reader.readAsDataURL(input.files[0]);
                        selected_file = input.files[0];

                      }
                    }

                    $("#filePhoto").change(function() {
                      readURL(this);
                    });

                    $('#frmPost').submit(function(e){

                        e.preventDefault();
                    

                        var data = new FormData();
                        data.append('file',selected_file);
                        data.append('content',$('#feedbox').val());
                        
                        addPosts(data).done(function(res){
                            console.log(res);

                            //var html = $('#tplFeeds').html();
                            var data = res.data;
                            var html = '<div class="row" id="feed">';
                                html +=   '<div class="col-md-2"><img src="'+data.profile_pic+'" class="img-circle" width="100%"/></div>';
                                html +=   '<div class="col-md-10" style="border:1px solid #d3d3d3;">';
                                html +=     '<div><b><a href="" class="name-link">'+data.name+'</a></b>';
                                html +=       '<div class="pull-right text-muted" id="delete"></div>';
                                html +=     '</div>';
                                html +=     '<div>';
                                html +=       '<p style="padding: 10px 0px 30px 0px">'+data.content+'</p>';
                                if(selected_file !== null){
                                html += '<img style="width:100%;height:200px" src="'+data.filename+'">';
                                }
                      
                                html +=     '</div>';  
                                html +=     '<div class="text-muted"> <small>posted: '+data.date_posted+'</small></div>';
                                html +=     '<div hidden id="commentsContainer" style="margin-top:10px"></div>';
                                html +=     '<div style = "margin-top:10px">';
                                html +=       '<form feeds-id="'+data.feeds_id+'" role="form" class="frmAddComment">';
                                html +=         '<div class="form-group">';
                                html +=           '<textarea rows="1" class="form-control add-comment" placeholder="Write your comments here"></textarea>';
                                html +=         '</div>';
                                html +=       '</form>';
                                html +=     '</div>';
                                html +=   '</div>';
                                html += '</div>';
                            $(html).prependTo('#feedContainer').hide().fadeIn();

                            $('#previewHolder').hide();
                            $('#filePhoto').val('');
                            $('#feedbox').val('');
                            $('#btnCancel').hide();
                            selected_file = null;
                        });

                    });
                    $('#btnAddPhotos').click(function(){
                       $('#filePhoto').click();
                    });

                    $('#btnCancel').click(function(){
                       $('#previewHolder').hide();
                       $('#filePhoto').val('');
                       $('#btnCancel').hide();
                       selected_file = null;
                    });

                });

            });