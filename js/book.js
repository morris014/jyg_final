function addAdult(length){
	 if(length > 0){
	 var html = '';
	 	for(var i = 0; i < length; i++){
		 	html += '<div class = "row">';
	        html += '<div class="col-md-6">';
	        html += '<div class="form-group" style="margin-bottom:0;">';
	        html += '<input class="form-control" placeholder="First Name" name="adults['+ i +'][firstname]" required>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-6">';
            html += '<div class="form-group" style="margin-bottom:0;">';
            html += '<input class="form-control" placeholder="Last Name"  name="adults['+ i +'][lastname]" required>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
		}

		$('#adultsContainer').html(html);
	 }else{
	 	$('#adultsContainer').html('');
	 }
	
}
function addInfant(length){
	 if(length > 0){
	 var html = '';
	 	for(var i = 0; i < length; i++){
		 	html += '<div class = "row">';
	        html += '<div class="col-md-6">';
	        html += '<div class="form-group" style="margin-bottom:0;">';
	        html += '<input class="form-control" placeholder="First Name" name="infants['+ i +'][firstname]" required>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-md-6">';
            html += '<div class="form-group" style="margin-bottom:0;">';
            html += '<input class="form-control" placeholder="Last Name"  name="infants['+ i +'][lastname]" required>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
		}

		$('#infantsContainer').html(html);
	 }else{
	 	$('#infantsContainer').html('');
	 }
	
}
(function(){
	$(document).ready(function(){
		$('#adults').bind('change paste keyup',function(){
			addAdult(this.value);
		});
		$('#infants').bind('change paste keyup',function(){
			addInfant(this.value);
		});
		$('#btnBookNow').click(function(){
				//$('#confirmModal').modal('show');	
			if(!document.getElementById('frmBook').checkValidity()){
				alert('Please fill up all the fields');
			}else{
				var data = $('#frmBook').serializeArray();
				var obj = {};
				for(var i in data){
					obj[data[i].name] = data[i].value;
				}
				console.log(obj);
				var no_of_adults = obj.no_of_adults;
				var no_of_infants = obj.no_of_infants;
				var price_per_head = obj.price_per_head;	
				var adults_total = no_of_adults * price_per_head;
				var infants_total = no_of_infants * price_per_head;
				var subtotal = adults_total + infants_total;

				$('#adults_no_of_guest').html(obj.no_of_adults);
				$('#adults_total').html(adults_total);
				$('#infants_no_of_guest').html(no_of_infants);
				$('#infants_total').html(infants_total);
				$('#subtotal').html(subtotal);
				$('#total').html(subtotal);
				$('#confirmModal').modal('show');	
			}
		});
		$('#btnConfirm').click(function(){
			$('#confirmModal').modal('hide');

			$('#frmBook').submit();
		});
	});

})();