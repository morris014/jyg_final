<?php include_once 'config.php'; ?>
<?php
    //INITIALIZE FIRST LOAD
    $sign_up_success = false;

    if(isset($_POST['sign_up'])){
       $name = $MySQLiconn->real_escape_string($_POST['name']);
       $email = $MySQLiconn->real_escape_string($_POST['email']);
       $password = $MySQLiconn->real_escape_string($_POST['password']);
       $contact_no = $MySQLiconn->real_escape_string($_POST['contact_no']);
       $birthdate = $MySQLiconn->real_escape_string($_POST['birthdate']);
       $user_type_id = 2;
      $SQL = $MySQLiconn->query("INSERT INTO user_account(name,email,password,contact_no,birthdate,user_type) VALUES('$name','$email','$password','$contact_no','$birthdate',$user_type_id)");
      
      if(!$SQL)
      {
       die($MySQLiconn->error);
      }else{
        $sign_up_success = true;
      } 
    }
?>  
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php include 'common/head.php'; ?>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <?php include 'common/header.php'; ?>

<div class="page-top" id="templatemo_contact">
</div> <!-- /.page-header -->


        <div class="contact-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <?php if($sign_up_success): ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> You have registered succesfully
                        </div>
                        <?php endif;?>

                        <h3 class="widget-title">Sign Up</h3>
                        <div class="contact-form">
                            <form name="contactform" id="contactform" action="#" method="post">
                                <p>
                                    <input name="name" type="text" id="name" placeholder="Your Name" required>
                                </p>
                                <p>
                                    <input name="email" type="email" id="email" placeholder="Your Email" required> 
                                </p>
                                <p>
                                    <input name="password" type="password" id="subject" placeholder="Password" required> 
                                </p>
                                <p>
                                    <input type="text" name="contact_no" id="message" placeholder="Contact No" required>
                                </p>
                                <p>
                                    <input type="date" name="birthdate" id="message" placeholder="Birthdate" required>
                                </p>
                                <input type="submit" class="mainBtn" id="submit" value="Sign Up" name="sign_up">
                            </form>
                        </div> <!-- /.contact-form -->
                    </div>
                </div>
            </div>
        </div>








        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="footer-logo">
                            <a href="index.html">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="copyright">
                            <span>Copyright &copy; 2084 <a href="#">Company Name</a></span>
                        </div>
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <ul class="social-icons">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                            <li><a href="#" class="fa fa-flickr"></a></li>
                            <li><a href="#" class="fa fa-rss"></a></li>
                        </ul>
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.site-footer -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Map -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        
        <!-- Google Map Init-->
        <script type="text/javascript">
            jQuery(function($){
                $('.first-map, .map-holder').gmap3({
                    marker:{
                        address: '16.8496189,96.1288854' 
                    },
                        map:{
                        options:{
                        zoom: 16,
                        scrollwheel: false,
                        streetViewControl : true
                        }
                    }
                });
            });
        </script>
        <!-- templatemo 409 travel -->
    </body>
</html>