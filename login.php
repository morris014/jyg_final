<?php include_once 'config.php'; ?>
<?php
    //INITIALIZE FIRST LOAD
    $login_success = false;
    $login_failed = false;
    
    if(isset($_POST['login'])){
       $redirect = $_GET['redirect'];

       $email = $MySQLiconn->real_escape_string($_POST['email']);
       $password = $MySQLiconn->real_escape_string($_POST['password']);
        $sel_user = "SELECT * FROM user_account where email='{$email}' AND password = '{$password}' LIMIT 1";
        $run_user = mysqli_query($MySQLiconn, $sel_user) or die($MySQLiconn->error);
        $details = mysqli_fetch_assoc($run_user);
        $check_user = mysqli_num_rows($run_user);
        if($check_user>0){
            $_SESSION['email']=$email;
            $_SESSION['name'] = $details['name'];
            $_SESSION['user_type'] = $details['user_type'];
            $_SESSION['user_id'] = $details['id'];
            $login_success = true;
                
            if($redirect){
                header("Location: ".$redirect); 
            }else{
                if($details['user_type'] == 2){
                    header("Location: ".BASE_URI.'user/home.php');
                }else if($details['user_type'] == 1){
                    header("Location: ".BASE_URI.'admin');
                }    
            }
            
        }

        else {
            $login_failed = true;
        }
      
    }
?>  
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <?php include 'common/head.php'; ?>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
        <?php include 'common/header.php'; ?>

<div class="page-top" id="templatemo_contact">
</div> <!-- /.page-header -->


        <div class="contact-page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4">
                        <h3 class="widget-title">Login</h3>
                        <div class="contact-form">
                             <?php if($login_success): ?>
                            <div class="alert alert-success">
                              <strong>Success!</strong> You have login succesfully
                            </div>
                            <?php endif;?>
                             <?php if($login_failed): ?>
                            <div class="alert alert-warning">
                              <strong>Login Failed!</strong> Invalid Email / Password
                            </div>
                            <?php endif;?>

                            <form name="contactform" id="contactform" action="#" method="post">
                                
                                <p>
                                    <input name="email" type="text" id="email" placeholder="Your Email" required> 
                                </p>
                                <p>
                                    <input name="password" type="password" id="subject" placeholder="Passsword" required> 
                                </p>
                                <input type="submit" class="mainBtn" name="login" id="submit" value="Login">
                            </form>
                        </div> <!-- /.contact-form -->
                           <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                            Don't have an account! 
                                        <a href="signup.php">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                            </div>    
                    </div>
                </div>
            </div>
        </div>








        <div class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="footer-logo">
                            <a href="index.html">
                                <img src="images/logo.png" alt="">
                            </a>
                        </div>
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="copyright">
                            <span>Copyright &copy; 2084 <a href="#">Company Name</a></span>
                        </div>
                    </div> <!-- /.col-md-4 -->
                    <div class="col-md-4 col-sm-4">
                        <ul class="social-icons">
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-linkedin"></a></li>
                            <li><a href="#" class="fa fa-flickr"></a></li>
                            <li><a href="#" class="fa fa-rss"></a></li>
                        </ul>
                    </div> <!-- /.col-md-4 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.site-footer -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Map -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        
        <!-- Google Map Init-->
        <script type="text/javascript">
            jQuery(function($){
                $('.first-map, .map-holder').gmap3({
                    marker:{
                        address: '16.8496189,96.1288854' 
                    },
                        map:{
                        options:{
                        zoom: 16,
                        scrollwheel: false,
                        streetViewControl : true
                        }
                    }
                });
            });
        </script>
        <!-- templatemo 409 travel -->
    </body>
</html>